# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

data = np.loadtxt('BC/Fe.projbands.dat')

rdata = data.reshape(281,-1,14)


x = rdata[:,0,0]
fig, ax = plt.subplots()

NORB = 10
for i in range(NORB):
    ax.eventplot(rdata[:,:,2], orientation="vertical", linelengths=1/NORB,lineoffsets=x+i/NORB, linewidth=10*rdata[:,:,i+4],colors=plt.cm.tab10(i))
ax.grid()
plt.show()


x = rdata[:,0,0]
fig, ax = plt.subplots()

NORBG = 4
ORBG = [[4],[5,6,7],[8,9,10,11,12],[13]]


for i in range(NORBG):
    ax.eventplot(rdata[:,:,2], orientation="vertical", linelengths=1/NORBG,lineoffsets=x+i/NORBG, linewidth=10*np.sum(rdata[:,:,ORBG[i]],axis=-1),colors=plt.cm.tab10(i))
ax.grid()
plt.show()



from lxml import etree, objectify
import glob

nkpt   = 281
nbnd   = 20


E=np.zeros([NORBG, nkpt, nbnd])
P=np.zeros([NORBG, nkpt, nbnd])
AP=np.zeros([NORBG, nkpt, nbnd])
orbs=['3S', '3P', '3D', '4S']

for i, orb in enumerate(orbs):
    
    for fname in glob.glob('SC/Fe_{}.dat.save/*.xml'.format(orb)):
        with open(fname,'r') as fobj:
            xml = fobj.read()
        data = objectify.fromstring(xml)
        ik = int(data.EIGENVALUES.attrib['ik'])
        ispin = int(data.EIGENVALUES.attrib['ispin'])
        E[i, ik-1, :]  = [float(x) for x in data.EIGENVALUES.text.split()]
        P[i, ik-1, :]  = [float(x) for x in data.PROJECTIONS.text.split()]
        AP[i, ik-1, :] = [float(x) for x in data.ATOMIC_PROJECTIONS.text.split()]


fig, [ax1, ax2] = plt.subplots(nrows=2, ncols=1)
for i in range(NORBG):
    ax1.eventplot(rdata[:,:,2], orientation="vertical", linelengths=1/NORBG,lineoffsets=x+i/NORBG, linewidth=10*np.sum(rdata[:,:,ORBG[i]],axis=-1),colors=plt.cm.tab10(i))
    ax2.eventplot(E[i], orientation="vertical", linelengths=1/NORBG,lineoffsets=x+i/NORBG, linewidth=(10*AP[i]*P[i]),colors=plt.cm.tab10(i))


l = []
for i, orb in enumerate(orbs):
    l.append(mpatches.Patch(color=plt.cm.tab10(i), label=orb))

ax1.legend(handles=l)
ax1.grid()
ax2.grid()
ax1.set_ylim([9,20])
ax2.set_ylim([9,20])
plt.show()
