!
! Copyright (C) 2013 Pietro Bonfa' 
! This file is distributed under the terms of the
! GNU General Public License. 
! See http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE atmproj_read()
    !----------------------------------------------------------------------------
    !
    ! read all atomic projections from file and store them into 
    ! module data
    !
    USE kinds,       ONLY: DP
    USE read_proj,   ONLY: read_xml_proj
    USE io_files,    ONLY: restart_dir
    USE klist,       ONLY: nks
    USE unfold_data, ONLY: projs
    USE lsda_mod,    ONLY: nspin
    USE wvfct,       ONLY: nbnd
    ! 
    IMPLICIT NONE
    !
    INTEGER  :: ierr, natomwfc, nbnd_loc, nspin_loc, nkstot_loc
    REAL(DP) :: nelec, ef
    REAL(DP),    ALLOCATABLE :: et(:,:), xk(:,:), wk(:)
    CHARACTER(200) :: filename

    !
    ! read projdos file
    !
    filename=TRIM( restart_dir())//"atomic_proj.xml"

    !
    ! these quantitites are allocated inside read_xml_proj
    !
    !ALLOCATE ( xk(3,nks) , wk(nks) )
    !ALLOCATE ( et(nbnd, nks) )
    !ALLOCATE ( projs(natomwfc, nbnd, nks) )
    !
    IF (ALLOCATED(xk)) deallocate(xk)
    IF (ALLOCATED(wk)) deallocate(wk)
    IF (ALLOCATED(et)) deallocate(et)
    IF (ALLOCATED(projs)) deallocate(projs)
    !
    CALL read_xml_proj( filename, ierr, natomwfc, nbnd_loc, nkstot_loc, &
                        nspin_loc, nelec, ef, xk, wk, et, projs )
    !
    ! a few checks
    !
    if (nbnd_loc /= nbnd)     CALL errore("atmproj_read","invalid nbnd from file",10)
    if (nspin_loc /= nspin)   CALL errore("atmproj_read","invalid nspin from file",10)
    if (nkstot_loc /= nks)    CALL errore("atmproj_read","invalid nks from file",10)
    !
    ! cleanup
    !
    DEALLOCATE( xk, wk, et)
    !
END SUBROUTINE atmproj_read
